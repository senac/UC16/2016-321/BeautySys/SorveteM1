package br.com.sorvete.bean;

import br.com.sorvete.banco.ProdutoDAO;
import br.com.sorvete.entity.Produto;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.faces.view.ViewScoped;

@Named(value = "produtoBean")
@ViewScoped
public class ProdutoBean extends Bean {

    private Produto produto;
    private ProdutoDAO dao;

    public ProdutoBean() {
    }

    @PostConstruct
    public void init() {
       this.dao = new ProdutoDAO();
       this.novo();
    }

    public String getCodigo() {
        return this.produto.getId() == 0 ? "" : String.valueOf(this.produto.getId());
    }

    public void novo() {
        this.produto = new Produto();
       
    }

    public void salvar() {

        try {

            if (this.produto.getId() == 0) {
                dao.save(produto);
                addMessageInfo("Salvo com sucesso!");
                novo();
            } else {
                dao.update(produto);
                addMessageInfo("Alterado com sucesso!");
                novo();
            }

        } catch (Exception ex) {
            addMessageInfo(ex.getMessage());
        }

    }  

    public void excluir(Produto produto) {
        try {
            dao.delete(produto.getId());
            addMessageInfo("Removido com sucesso!");

        } catch (Exception ex) {
            addMessageErro(ex.getMessage());
        }
    }

    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    public List<Produto> getLista() {
        return this.dao.findAll();
    }
   
}

